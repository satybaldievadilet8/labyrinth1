using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHealthBehaviour : MonoBehaviour, IComponent, IHealth
{
    private CharacterMainBehaviour characterMainBehaviour;

    private int currentHealth;
    public int CurrentHealth
    {
        get { return currentHealth; }
        set
        {
            currentHealth = value;
            if (currentHealth > 0)
            {
                currentHealth = value;
            }
            else
            {
                currentHealth = 0;
                characterMainBehaviour.SetState(CharacterStateType.death);
            }
        }
    }
    private int maxHealth;

    private bool isAlive()
    {
        if (characterMainBehaviour.characterState != CharacterStateType.death)
            return true;
        else
            return false;
    }

    public virtual void Initialize(IComponent component)
    {
        characterMainBehaviour = component as CharacterMainBehaviour;
    }

    protected virtual void OnEnable()
    {
        characterMainBehaviour.OnSetState += OnSetState;
    }
    protected virtual void OnDisable()
    {
        characterMainBehaviour.OnSetState -= OnSetState;
    }

    protected virtual void OnSetState(CharacterStateType stateType)
    {
        switch (stateType)
        {
            case CharacterStateType.revival:
                Revival();
                break;
        }
    }

    protected virtual void Revival()
    {
        CurrentHealth = maxHealth;
    }

    private void Start()
    {
        InitializeLink();
    }
    protected virtual void InitializeLink()
    {
        var config = characterMainBehaviour.GetConfig();

        maxHealth = config.maxHealth;
        CurrentHealth = maxHealth;
    }
    public virtual void TakeDamage(int damage)
    {
        if (!isAlive()) return;

        CurrentHealth -= damage;
    }
}
