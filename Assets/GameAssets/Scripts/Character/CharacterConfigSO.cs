using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/Character", order = 51)]
public class CharacterConfigSO : ScriptableObject
{
    [field: SerializeField] public int maxHealth { get; protected set; }
}
