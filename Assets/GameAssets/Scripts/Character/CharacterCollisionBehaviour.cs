using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCollisionBehaviour : MonoBehaviour, IComponent, ICollision
{
    private CharacterMainBehaviour characterMainBehaviour;

    private Collider mainCollider;

    public CharacterMainBehaviour GetCharacterMain()
    {
        return characterMainBehaviour;
    }

    public virtual void Initialize(IComponent component)
    {
        characterMainBehaviour = component as CharacterMainBehaviour;
    }

    private void Start()
    {
        InitializeLink();
    }

    private void InitializeLink()
    {
        mainCollider = GetComponent<Collider>();
    }

    public virtual void Activate()
    {
        mainCollider.enabled = true;
    }

    public virtual void Deactivate()
    {
        mainCollider.enabled = false;
    }


}
