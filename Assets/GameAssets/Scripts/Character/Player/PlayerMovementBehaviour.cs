using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerMovementBehaviour : CharacterMovementBehaviour
{
    public static event Action OnFinish;

    #region SerializeField
    [SerializeField] private Transform startPointRay;
    [SerializeField] private LayerMask cellLayer;
    [SerializeField] private LayerMask obstaclesWallLayer;
    #endregion

    #region Variable
    private PlayerMainBehaviour playerMainBehaviour;

    private Vector3 moveToPoint;
    private Vector3 targetPosition;
    private Vector3 finishPosition;

    private List<Vector2> previousTargetCoordinatesList = new List<Vector2>();

    private Vector3[] diractionRays = new Vector3[4]
    {
        Vector3.forward, Vector3.back, Vector3.left,  Vector3.right,  
    };

    private bool isFindFinish = true;
    #endregion

    public override void Initialize(IComponent component)
    {
        base.Initialize(component);
        playerMainBehaviour = component as PlayerMainBehaviour;
    }

    private void Start()
    {
        moveToPoint = transform.position;
        finishPosition = MapManager.GetPosition(playerMainBehaviour.GetTargetPosition());
        finishPosition.y = transform.position.y;
    }

    protected override void MoveForward()
    {
        base.MoveForward();

        if (transform.position == finishPosition)
        {
            print("finish");
            //OnFinish?.Invoke();
        }

        FindPath();

        if (transform.position != moveToPoint)
        {
            transform.position = Vector3.MoveTowards(transform.position, moveToPoint, 2 * Time.deltaTime);
        }
        else
        {
            moveToPoint = targetPosition;
        }
    }

    private void FindPath()
    {
        if (!isFindFinish) return;

        var cellCoordinates = new List<Vector2>(diractionRays.Length);
        var hit = new RaycastHit();
        foreach (var item in diractionRays)
        {
            Debug.DrawRay(startPointRay.position, item * 100, Color.red);
            if (Physics.Raycast(startPointRay.position, item * 100, out hit, obstaclesWallLayer))
            {
                if (hit.transform.TryGetComponent(out FinishCollisionBehaviour finishColl))
                {
                    var finishPosition = MapManager.GetPosition(finishColl.GetFinishMainBehaviour().GetCoordinates());
                    if (moveToPoint != finishPosition)
                    {
                        NearCoordinates();
                        this.targetPosition = finishPosition;
                        isFindFinish = false;
                    }
                    return;
                }

                var startPoint = hit.transform.position - (item / 2);
                Debug.DrawRay(startPoint, -hit.transform.up * 100, Color.green);
                if (Physics.Raycast(startPoint, -hit.transform.up * 100, out hit, cellLayer))
                {
                    if (hit.transform.TryGetComponent(out CellCollisionBehaviour collision))
                    {
                        cellCoordinates.Add(collision.GetCellMainBehaviour().coordinates);
                    }
                    else if (hit.transform.TryGetComponent(out CellWallCollisionBehaviour wallCollision))
                    {
                        cellCoordinates.Add(wallCollision.GetCellMainBehaviour().coordinates);
                    }
                }
            }
        }

        var targetPosition = GetPosition(cellCoordinates);
        if (moveToPoint != targetPosition)
        {
            NearCoordinates();
            this.targetPosition = targetPosition;
        }

        if (transform.position == moveToPoint)
        {
            this.targetPosition = GetAnotherPosition(cellCoordinates);
        }
    }
    private Vector3 GetPosition(List<Vector2> coordinates)
    {
        var targetCoordinates = new List<Vector2>();
        var distances = new List<float>();
        var currentCoordinates = Vector2.zero;
        var hit = new RaycastHit();
        if (Physics.Raycast(transform.position, -transform.up * 100, out hit, cellLayer))
        {
            if (hit.transform.TryGetComponent(out CellCollisionBehaviour collision))
            {
                currentCoordinates = collision.GetCellMainBehaviour().coordinates;
            }
        }

        foreach (var item in coordinates)
        {
            if (previousTargetCoordinatesList.Contains(item) || currentCoordinates == item)
                continue;

            targetCoordinates.Add(item);
            distances.Add(Vector3.Distance(finishPosition, MapManager.GetPosition(item)));
        }

        float minDistance = distances.Min();

        for (int i = 0; i < distances.Count; i++)
        {
            if (distances[i] == minDistance)
            {
                var position = MapManager.GetPosition(targetCoordinates[i]);
                return new Vector3(position.x, transform.position.y, position.z);
            }
        }
        print("zero");
        return transform.position;
    }

    private Vector3 GetAnotherPosition(List<Vector2> coordinates)
    {
        var targetCoordinates = new List<Vector2>();
        var distances = new List<float>();
        var currentCoordinates = Vector2.zero;
        var hit = new RaycastHit();
        if (Physics.Raycast(transform.position, -transform.up * 100, out hit, cellLayer))
        {
            if (hit.transform.TryGetComponent(out CellCollisionBehaviour collision))
            {
                currentCoordinates = collision.GetCellMainBehaviour().coordinates;
            }
        }

        foreach (var item in coordinates)
        {
            if (previousTargetCoordinatesList.Contains(item) || currentCoordinates == item)
                continue;

            targetCoordinates.Add(item);
            distances.Add(Vector3.Distance(transform.position, MapManager.GetPosition(item)));
        }

        float minDistance = distances.Max();

        for (int i = 0; i < distances.Count; i++)
        {
            if (distances[i] == minDistance)
            {
                var position = MapManager.GetPosition(targetCoordinates[i]);
                return new Vector3(position.x, transform.position.y, position.z);
            }
        }
        print("zero");
        return transform.position;
    }
   
    private void NearCoordinates()
    {
        var hit = new RaycastHit();
        if (Physics.Raycast(transform.position, -transform.up * 100, out hit, cellLayer))
        {
            if (hit.transform.TryGetComponent(out CellCollisionBehaviour collision))
            {
                var position = MapManager.GetPosition(collision.GetCellMainBehaviour().coordinates);
                moveToPoint = new Vector3(position.x, transform.position.y, position.z);
            }
        }
    }
}
