using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerNavMeshBehaviour : CharacterMovementBehaviour
{
    public static event Action OnFinish;

    private PlayerMainBehaviour playerMainBehaviour;
    private NavMeshAgent navMeshAgent;
    private Vector3 targetPosition;

    public override void Initialize(IComponent component)
    {
        base.Initialize(component);
        playerMainBehaviour = component as PlayerMainBehaviour;
    }

    protected override void OnSetState(CharacterStateType stateType)
    {
        base.OnSetState(stateType);
        switch (stateType)
        {
            case CharacterStateType.death:
                navMeshAgent.enabled = false;
                enabled = false;
                break;
        }
    }

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        targetPosition = playerMainBehaviour.GetTargetPosition();
        Stop();
        StartCoroutine(CooldownCoroutine());
    }
    private IEnumerator CooldownCoroutine()
    {
        yield return new WaitForSeconds(2);
        Move();
    }

    protected override void MoveForward()
    {
        base.MoveForward();
        if (Vector3.Distance(transform.position, targetPosition) < .2f)
        {
            OnFinish?.Invoke();
            return;
        }

        navMeshAgent.SetDestination(targetPosition);
    }
}
