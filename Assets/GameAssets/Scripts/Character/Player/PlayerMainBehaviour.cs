using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMainBehaviour : CharacterMainBehaviour
{
    public static event Action OnDeath;

    [SerializeField] private ParticleSystem conffetyParticle;
    [SerializeField] private Color shieldColor;

    private PlayerCollisionBehaviour playerCollisionBehaviour;
    private PlayerVisualBehaviour playerVisualBehaviour;

    private Vector3 targetPosition;

    protected override void Awake()
    {
        base.Awake();
        InitializeLink();
    }
    private void InitializeLink()
    {
        playerCollisionBehaviour = GetComponentInGameObject<PlayerCollisionBehaviour>();
        playerVisualBehaviour = GetComponentInGameObject<PlayerVisualBehaviour>();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        GameManager.OnSetState += OnSetStateGameManager;
        UISkill.OnTouch += OnTouchSKillBtn;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        GameManager.OnSetState -= OnSetStateGameManager;
        UISkill.OnTouch -= OnTouchSKillBtn;
    }

    private void OnSetStateGameManager(GameStateType stateType)
    {
        switch (stateType)
        {
            case GameStateType.finish:
                SetState(CharacterStateType.disable);
                conffetyParticle.Play();
                break;
        }
    }

    private void OnTouchSKillBtn(bool isTouch)
    {
        if (isTouch)
        {
            playerVisualBehaviour.SetColor(shieldColor);
            playerCollisionBehaviour.Deactivate();
            StopAllCoroutines();
            StartCoroutine(SkillCooldown());
        }
        else
        {
            playerVisualBehaviour.RestartColor();
            playerCollisionBehaviour.Activate();
        }
    }
    private IEnumerator SkillCooldown()
    {
        yield return new WaitForSeconds(2);
        playerVisualBehaviour.RestartColor();
        playerCollisionBehaviour.Activate();
    }

    public Vector3 GetTargetPosition()
    {
        return targetPosition;
    }
    public void SetTargetPosition(Vector3 position)
    {
        targetPosition = position;
    }
    protected override void Death()
    {
        base.Death();
        StartCoroutine(DeathCoroutine());
    }
    private IEnumerator DeathCoroutine()
    {
        yield return new WaitForSeconds(2);
        OnDeath?.Invoke();
        Destroy(gameObject);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            SetState(CharacterStateType.death);
        }
    }
}
