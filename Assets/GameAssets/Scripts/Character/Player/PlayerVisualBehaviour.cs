using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVisualBehaviour : CharacterVisualBehaviour
{
    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private Transform cubeParent;

    private PlayerMainBehaviour playerMainBehaviour;

    private Color baseColor;

    public override void Initialize(IComponent component)
    {
        base.Initialize(component);
        playerMainBehaviour = component as PlayerMainBehaviour;
    }

    private void OnEnable()
    {
        playerMainBehaviour.OnSetState += OnSetState;
    }
    private void OnDisable()
    {
        playerMainBehaviour.OnSetState -= OnSetState;
    }

    private void OnSetState(CharacterStateType stateType)
    {
        switch (stateType)
        {
            case CharacterStateType.death:
                meshRenderer.enabled = false;
                for (int i = 0; i < cubeParent.childCount; i++)
                {
                    cubeParent.GetChild(i).gameObject.SetActive(true);
                    cubeParent.GetChild(i).GetComponent<Rigidbody>().AddExplosionForce(200, transform.position, 5);
                }
                break;
        }

    }

    private void Start()
    {
        baseColor = meshRenderer.material.color;
    }

    public void SetColor(Color color)
    {
        meshRenderer.material.DORewind();
        meshRenderer.material.DOColor(color, .5f);
    }

    public void RestartColor()
    {
        meshRenderer.material.DORewind();
        meshRenderer.material.DOColor(baseColor, .5f);
    }
}
