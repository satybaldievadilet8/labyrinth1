using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementBehaviour : MonoBehaviour, IComponent, IMove
{
    private CharacterMainBehaviour characterMainBehaviour;

    protected bool isMove;

    public virtual void Initialize(IComponent component)
    {
        characterMainBehaviour = component as CharacterMainBehaviour;
    }

    protected virtual void OnEnable()
    {
        characterMainBehaviour.OnSetState += OnSetState;
    }
    protected virtual void OnDisable()
    {
        characterMainBehaviour.OnSetState -= OnSetState;
    }

    protected virtual void OnSetState(CharacterStateType stateType)
    {
        switch (stateType)
        {
            case CharacterStateType.revival:
                Move();
                break;
            case CharacterStateType.disable:
                Stop();
                break;
            case CharacterStateType.death:
                Stop();
                break;
        }
    }

    public virtual void Move()
    {
        isMove = true;
    }
    public virtual void Stop()
    {
        isMove = false;
    }

    private void Update()
    {
        if (!isMove) return;

        MoveForward();
    }

    protected virtual void MoveForward()
    {

    }
}
