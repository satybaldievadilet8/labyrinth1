using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMainBehaviour : MainBehaviour, IComponent
{
    public event Action<CharacterStateType> OnSetState;
    public CharacterStateType characterState { get; private set; }

    [SerializeField] private CharacterConfigSO characterConfigSO;

    public CharacterConfigSO GetConfig()
    {
        return characterConfigSO;
    }

    protected override void Awake()
    {
        base.Awake();
        Initialize(this);
    }
    public void Initialize(IComponent component)
    {
        components.ForEach(item => item.Initialize(component));
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        StartCoroutine(RevivalCoroutine());
    }
    private IEnumerator RevivalCoroutine()
    {
        yield return new WaitForEndOfFrame();
        SetState(CharacterStateType.revival);
    }

    public virtual void SetState(CharacterStateType state)
    {
        if (characterState == state) return;

        switch (state)
        {
            case CharacterStateType.revival:
                Revival();
                break;
            case CharacterStateType.disable:
                Disable();
                break;
            case CharacterStateType.death:
                Death();
                break;
        }

        characterState = state;
        OnSetState?.Invoke(characterState);
    }

    protected virtual void Revival()
    {

    }
    protected virtual void Disable()
    {

    }
    protected virtual void Death()
    {

    }
}
