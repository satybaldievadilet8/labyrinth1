public interface IComponent
{
    public void Initialize(IComponent component);
}

public interface IMove
{
    public void Move();
    public void Stop();
}

public interface ICollision
{
    public void Activate();
    public void Deactivate();
}

public interface IAttack
{
    public void Attack();
    public void ApplyDamage();
}

public interface IHealth
{
    public void TakeDamage(int damage);
}