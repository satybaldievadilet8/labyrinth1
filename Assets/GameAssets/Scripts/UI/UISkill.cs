using System;
using UnityEngine;

public class UISkill : MonoBehaviour
{
    public static event Action<bool> OnTouch;
    public void SkillDownBtn()
    {
        OnTouch?.Invoke(true);
    }
    public void SkillUpBtn()
    {
        OnTouch?.Invoke(false);
    }

}
