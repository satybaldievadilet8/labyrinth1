using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMenu : MonoBehaviour
{
    public static event Action<bool> OnMenu;
    public static event Action OnExit;

    [SerializeField] private Button entranceBtn;
    [SerializeField] private Button resumeBtn;
    [SerializeField] private Button exitBtn;

    [Space]
    [SerializeField] private Image background;
    [SerializeField] private Image menuBackground;


    private Color startBackgroundColor;
    private Color startMenuBackgroundColor;

    private void Start()
    {
        Initialize();

        menuBackground.gameObject.SetActive(false);
        background.gameObject.SetActive(false);
    }
    private void Initialize()
    {
        entranceBtn.onClick.AddListener(EntranceBtn);
        resumeBtn.onClick.AddListener(ResumeBtn);
        exitBtn.onClick.AddListener(ExitBtn);

        startBackgroundColor = background.color;
        startMenuBackgroundColor = menuBackground.color;
    }

    public void EntranceBtn()
    {
        background.DORewind();
        menuBackground.DORewind();
        resumeBtn.DORewind();
        exitBtn.DORewind();

        background.color = new Color(background.color.r, background.color.g, background.color.b, 0);
        menuBackground.color = new Color(menuBackground.color.r, menuBackground.color.g, menuBackground.color.b, 0);

        resumeBtn.transform.localScale = Vector3.zero;
        exitBtn.transform.localScale = Vector3.zero;

        background.gameObject.SetActive(true);
        menuBackground.gameObject.SetActive(true);

        background.DOColor(startBackgroundColor, .4f).SetUpdate(true).OnComplete(() =>
        {
            menuBackground.DOColor(startMenuBackgroundColor, .2f).SetUpdate(true).OnComplete(() =>
            {
                resumeBtn.transform.DOScale(Vector3.one, .3f).SetUpdate(true).OnComplete(() => resumeBtn.transform.DOPunchScale(Vector3.one * .2f, .2f).SetUpdate(true));

                exitBtn.transform.DOScale(Vector3.one, .3f).SetDelay(.2f).SetUpdate(true).OnComplete(() => exitBtn.transform.DOPunchScale(Vector3.one * .2f, .2f).SetUpdate(true));
            });
        });

        OnMenu?.Invoke(true);
    }
    public void ResumeBtn()
    {
        background.DORewind();

        var currentColor = background.color;
        currentColor.a = 0;

        background.DOColor(currentColor, .4f).SetUpdate(true).OnComplete(() => 
        {
            background.gameObject.SetActive(false);
            OnMenu?.Invoke(false);
        });
        menuBackground.gameObject.SetActive(false);
    }
    public void ExitBtn()
    {
        OnExit?.Invoke();
    }

    private void OnDestroy()
    {
        entranceBtn.onClick.RemoveListener(EntranceBtn);
        resumeBtn.onClick.RemoveListener(EntranceBtn);
        exitBtn.onClick.RemoveListener(ExitBtn);
    }
}
