using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILoadiongScreen : MonoBehaviour
{
    public static event Action<bool> OnLoadingScreen;

    private Image screen;

    private Color baseColor;

    private void Awake()
    {
        screen = transform.GetChild(0).GetComponent<Image>();
        baseColor = screen.color;
    }

    private void OnEnable()
    {
        GameManager.OnSetState += OnSetState;
    }
    private void OnDisable()
    {
        GameManager.OnSetState -= OnSetState;
    }

    private void OnSetState(GameStateType stateType)
    {
        switch (stateType)
        {
            case GameStateType.start:
                LoadingScreen(false);
                break;
            case GameStateType.finish:
                StartCoroutine(LoadingScreenCoroutine());
                break;
        }
    }
    private IEnumerator LoadingScreenCoroutine()
    {
        yield return new WaitForSeconds(2);
        LoadingScreen(true);
    }


    private void LoadingScreen(bool isShow)
    {
        screen.DORewind();
        if (isShow)
        {
            screen.gameObject.SetActive(true);
            screen.color = Vector4.zero;
            var targetColor = baseColor;
            targetColor.a = 1;
            screen.DOColor(targetColor, 1).SetUpdate(true).OnComplete(() => OnLoadingScreen?.Invoke(false));
        }
        else
        {
            screen.color = baseColor;
            var targetColor = baseColor;
            targetColor.a = 0;
            screen.DOColor(targetColor, 1).SetUpdate(true).OnComplete(() => screen.gameObject.SetActive(false));
        }
    }
}
