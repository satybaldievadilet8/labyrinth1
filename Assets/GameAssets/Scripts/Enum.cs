public enum GameStateType
{
    nothing,
    loading,
    start,
    restart,
    loadingNewLevel,
    resume,
    pause,
    finish,
    quit
}

public enum CharacterStateType
{
    nothing,
    revival,
    movement,
    disable,
    death
}

public enum TrapStateType
{
    nothing,
    revival,
    attack,
    death
}

public enum DifficultyLevelType
{
    nothing,
    easy,
    medium,
    hard
}

public enum ObstaclesType
{
    nothing,
    road,
    roadWall,
    fireRoad
}