using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapMainBehaviour : ObstaclesMainBehaviour
{
    public event Action<TrapStateType> OnSetState;
    public TrapStateType trapState { get; private set; }

    private List<CharacterMainBehaviour> characterTarget = new List<CharacterMainBehaviour>();

    private TrapCollisionBehaviour collisionBehaviour;

    private void ClearCharacterNull()
    {
        for (int i = 0; i < characterTarget.Count; i++)
        {
            if (!characterTarget[i] || characterTarget[i].characterState == CharacterStateType.death)
                characterTarget.Remove(characterTarget[i]);
        }
    }

    public CharacterMainBehaviour GetCharacterTarget()
    {
        ClearCharacterNull();
        if (characterTarget.Count == 0) return null;
        else return characterTarget[0];
    }

    protected override void Awake()
    {
        base.Awake();
        InitializeLink();
    }

    protected virtual void InitializeLink()
    {
        collisionBehaviour = GetComponentInGameObject<TrapCollisionBehaviour>();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        collisionBehaviour.OnCharacterCollision += OnCharacterCollision;
        StartCoroutine(RevivalCoroutine());
    }
    private IEnumerator RevivalCoroutine()
    {
        yield return new WaitForEndOfFrame();
        SetState(TrapStateType.revival);
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        collisionBehaviour.OnCharacterCollision -= OnCharacterCollision;
    }

    public virtual void SetState(TrapStateType state)
    {
        if (trapState == state) return;

        switch (state)
        {
            case TrapStateType.revival:
                Revival();
                break;
        }

        trapState = state;
        OnSetState?.Invoke(trapState);
    }

    protected virtual void Revival()
    {

    }
    private void OnCharacterCollision(CharacterMainBehaviour character)
    {
        characterTarget.Add(character);
    }
}
