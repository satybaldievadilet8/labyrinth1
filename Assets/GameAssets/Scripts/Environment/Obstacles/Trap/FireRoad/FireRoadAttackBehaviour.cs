using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireRoadAttackBehaviour : TrapAttackBehaviour
{
    private FireRoadMainBehavour fireRoadMainBehavour;

    public override void Initialize(IComponent component)
    {
        base.Initialize(component);
        fireRoadMainBehavour = component as FireRoadMainBehavour;
    }
}
