using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapAttackBehaviour : MonoBehaviour, IComponent, IAttack
{
    private TrapMainBehaviour trapMainBehaviour;

    private int currentDamage;

    private float cooldownAttack, countCooldownAttack;

    private bool isAttack;

    public virtual void Initialize(IComponent component)
    {
        trapMainBehaviour = component as TrapMainBehaviour;
    }

    protected virtual void OnEnable()
    {
        trapMainBehaviour.OnSetState += OnSetState;
    }
    protected virtual void OnDisable()
    {
        trapMainBehaviour.OnSetState -= OnSetState;
    }

    protected virtual void OnSetState(TrapStateType stateType)
    {
        switch (stateType)
        {
            case TrapStateType.revival:
                isAttack = true;
                break;
            case TrapStateType.death:
                isAttack = false;
                break;
        }
    }

    private void Start()
    {
        InitializeLink();
    }
    protected virtual void InitializeLink()
    {
        var config = trapMainBehaviour.GetConfig() as TrapConfigSO;

        cooldownAttack = config.cooldownAttack;
        currentDamage = config.damage;
    }

    private void Update()
    {
        if (!isAttack) return;

        Attack();
    }
    public virtual void Attack()
    {
        if (!trapMainBehaviour.GetCharacterTarget()) return;

        Cooldown();
    }
    private void Cooldown()
    {
        countCooldownAttack += Time.deltaTime;

        if (cooldownAttack < countCooldownAttack)
        {
            ApplyDamage();
            countCooldownAttack = 0;
        }
    }
    public virtual void ApplyDamage()
    {
        var character = trapMainBehaviour.GetCharacterTarget();
        if (!character) return;

        character.GetComponentInGameObject<IHealth>().TakeDamage(currentDamage);
    }
}
