using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapCollisionBehaviour : MonoBehaviour, IComponent, ICollision
{
    public event Action<CharacterMainBehaviour> OnCharacterCollision;

    private TrapMainBehaviour trapMainBehaviour;


    public virtual void Initialize(IComponent component)
    {
        trapMainBehaviour = component as TrapMainBehaviour;
    }
    public void Activate()
    {

    }

    public void Deactivate()
    {

    }

    protected virtual void CharacterCollision(CharacterCollisionBehaviour character)
    {
        OnCharacterCollision?.Invoke(character.GetCharacterMain());
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out CharacterCollisionBehaviour characterCollisionBehaviour))
        {
            CharacterCollision(characterCollisionBehaviour);
        }
    }
}
