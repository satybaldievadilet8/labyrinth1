using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/Obstacles/Trap", order = 51)]
public class TrapConfigSO : ObstaclesConfigSO
{
    [field: SerializeField] public int damage { get; protected set; }
    [field: SerializeField] public float cooldownAttack { get; protected set; }
}
