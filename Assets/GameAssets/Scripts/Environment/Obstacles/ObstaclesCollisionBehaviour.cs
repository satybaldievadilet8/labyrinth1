using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesCollisionBehaviour : MonoBehaviour, IComponent, ICollision
{
    private ObstaclesMainBehaviour obsatclesMainBehaviour;
    public ObstaclesMainBehaviour GetMainBehaviour()
    {
        return obsatclesMainBehaviour;
    }
    public void Activate()
    {
    }

    public void Deactivate()
    {
    }

    public virtual void Initialize(IComponent component)
    {
        obsatclesMainBehaviour = component as ObstaclesMainBehaviour;
    }
}
