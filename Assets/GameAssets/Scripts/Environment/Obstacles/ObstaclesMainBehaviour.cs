using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesMainBehaviour : MainBehaviour, IComponent
{
    [field: SerializeField] public ObstaclesType obstaclesType { get; protected set; }

    [SerializeField] protected ObstaclesConfigSO config;

    public ObstaclesConfigSO GetConfig()
    {
        return config;
    }

    protected override void Awake()
    {
        base.Awake();
        Initialize(this);
    }
    public virtual void Initialize(IComponent component)
    {
        components.ForEach(item => item.Initialize(component));
    }
}
