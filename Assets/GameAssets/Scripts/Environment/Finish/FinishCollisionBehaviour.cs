using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishCollisionBehaviour : MonoBehaviour, IComponent, ICollision
{
    public event Action<CharacterMainBehaviour> OnCollisionCharacter;

    private FinishMainBehaviour finishMainBehaviour;

    public FinishMainBehaviour GetFinishMainBehaviour()
    {
        return finishMainBehaviour;
    }

    public void Activate()
    {
    }

    public void Deactivate()
    {
    }

    public void Initialize(IComponent component)
    {
        finishMainBehaviour = component as FinishMainBehaviour;
    }

    private void CollisionCharacter(CharacterMainBehaviour characterMainBehaviour)
    {
        //OnCollisionCharacter?.Invoke(characterMainBehaviour);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out CharacterCollisionBehaviour characterCollisionBehaviour))
        {
            CollisionCharacter(characterCollisionBehaviour.GetCharacterMain());
        }
    }
}
