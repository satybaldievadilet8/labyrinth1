using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishMainBehaviour : MainBehaviour, IComponent
{
    public static event Action OnFinish;

    private FinishCollisionBehaviour finishCollisionBehaviour;

    private Vector2 coordinates;

    protected override void Awake()
    {
        base.Awake();
        Initialize(this);
        InitializeLink();
    }
    public void Initialize(IComponent component)
    {
        components.ForEach(item => item.Initialize(component));
    }
    private void InitializeLink()
    {
        finishCollisionBehaviour = GetComponentInGameObject<FinishCollisionBehaviour>();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        finishCollisionBehaviour.OnCollisionCharacter += OnCollisionCharacter;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        finishCollisionBehaviour.OnCollisionCharacter -= OnCollisionCharacter;
    }

    private void OnCollisionCharacter(CharacterMainBehaviour characterMainBehaviour)
    {
        if (characterMainBehaviour as PlayerMainBehaviour)
            OnFinish?.Invoke();
    }

    public Vector2 GetCoordinates()
    {
        return coordinates;
    }
    public void SetCoordinates(Vector2 coordinates)
    {
        this.coordinates = coordinates;
    }


}
