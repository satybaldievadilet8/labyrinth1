using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellVisualBehaviour : MonoBehaviour, IComponent
{
    private CellMainBehaviour cellMainBehaviour;

    public void Initialize(IComponent component)
    {
        cellMainBehaviour = component as CellMainBehaviour;
    }
}
