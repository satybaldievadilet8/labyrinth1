using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellMainBehaviour : MainBehaviour, IComponent
{
    [SerializeField] private TMPro.TextMeshPro _text;

    [SerializeField] private GameObject[] walls;

    public Vector2 coordinates { get; private set; }

    public ObstaclesMainBehaviour currentObstacles {get; private set;}

    protected override void Awake()
    {
        base.Awake();
        Initialize(this);
    }

    public void Initialize(IComponent component)
    {
        components.ForEach(item => item.Initialize(component));
    }

    public void SetCoordinates(Vector2 coordinates)
    {
        this.coordinates = coordinates;
        _text.text = coordinates.x.ToString() + "." + coordinates.y.ToString();
    }

    public void ShowWall(List<Vector3> direction)
    {
        foreach (var item in direction)
        {
            if (item == Vector3.right)
                walls[0].SetActive(true);
            else if (item == Vector3.left)
                walls[1].SetActive(true);
            else if (item == Vector3.forward)
                walls[2].SetActive(true);
            else if (item == Vector3.back)
                walls[3].SetActive(true);
        }
    }
    public void SetObstacles(ObstaclesMainBehaviour obstaclesMainBehaviour)
    {
        currentObstacles = obstaclesMainBehaviour;
    }
}
