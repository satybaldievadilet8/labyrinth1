using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellCollisionBehaviour : MonoBehaviour, IComponent, ICollision
{
    private CellMainBehaviour cellMainBehaviour;
    public CellMainBehaviour GetCellMainBehaviour()
    {
        return cellMainBehaviour;
    }
    public void Activate()
    {
    }

    public void Deactivate()
    {
    }

    public void Initialize(IComponent component)
    {
        cellMainBehaviour = component as CellMainBehaviour;
    }
}
