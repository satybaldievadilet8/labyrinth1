using System.Collections;
using System.Collections.Generic;
using Unity.AI.Navigation;
using UnityEngine;

public class NavMeshManager : MonoBehaviour, IManager
{
    private NavMeshSurface navMeshSurface;

    public void Initialize()
    {
        navMeshSurface = GetComponent<NavMeshSurface>();
    }

    public void Bake()
    {
        navMeshSurface.BuildNavMesh();
    }
}
