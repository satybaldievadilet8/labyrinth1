using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour, IManager
{
    public static event Action OnRestart;

    [SerializeField] private DifficultyLevelType difficultyLevelType;

    public void Initialize()
    {

    }

    private void OnEnable()
    {
        GameManager.OnSetState += OnSetState;
    }
    private void OnDisable()
    {
        GameManager.OnSetState -= OnSetState;
    }

    private void OnSetState(GameStateType gameStateType)
    {
        var mapManager = GameManager.Instance.GetManager<MapManager>();
        switch (gameStateType)
        {
            case GameStateType.start:
                mapManager.GenerationMap(difficultyLevelType);
                GameManager.Instance.GetManager<NavMeshManager>().Bake();
                break;
            case GameStateType.restart:
                mapManager.SpawnPlayer();
                OnRestart?.Invoke();
                break;
            case GameStateType.loadingNewLevel:
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                break;
            default:
                break;
        }
    }
}
