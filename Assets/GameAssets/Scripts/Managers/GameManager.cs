using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public interface IManager
{
    public void Initialize();
}

public class GameManager : MonoBehaviour, IManager
{
    public static GameManager Instance;
    public static event Action<GameStateType> OnSetState;
    public static GameStateType GameStateType { get; private set; }


    private List<GameObject> objectManagers = new List<GameObject>();

    public T GetManager<T>()
    {
        foreach (var item in objectManagers)
        {
            var manager = item.GetComponent<T>();
            if (manager == null) continue;
            return manager;
        }
        return default;
    }


    private void Awake()
    {
        Application.targetFrameRate = 120;
        Instance = this;
        SetState(GameStateType.loading);
        InitializeLink();
        Initialize();
    }
    private void InitializeLink()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            objectManagers.Add(transform.GetChild(i).gameObject);
        }
    }
    public void Initialize()
    {
        foreach (var item in objectManagers)
        {
            var manager = item.GetComponent<IManager>();
            if(manager == null) continue;
            manager.Initialize();
        }
    }
    private void SetState(GameStateType state)
    {
        if (GameStateType == state) return;

        switch (state)
        {
            case GameStateType.nothing:
                break;
            case GameStateType.loading:
                break;
            case GameStateType.start:

                break;
            case GameStateType.resume:
                break;
            case GameStateType.pause:
                break;
            case GameStateType.finish:
                break;
            case GameStateType.quit:
                Quit();
                break;
            default:
                break;
        }

        GameStateType = state;
        print(GameStateType);
        OnSetState?.Invoke(state);
    }

    private void Start()
    {
        SetState(GameStateType.start);
    }

    private void OnEnable()
    {
        UIMenu.OnMenu += OnMenu;
        UIMenu.OnExit += OnExit;
        FinishMainBehaviour.OnFinish += OnFinish;
        PlayerMainBehaviour.OnDeath += OnDeathPlayer;
        PlayerMovementBehaviour.OnFinish += OnFinish;
        PlayerNavMeshBehaviour.OnFinish += OnFinish;
        LevelManager.OnRestart += OnRestartLevel;
        UILoadiongScreen.OnLoadingScreen += OnLoadingScreen;
    }
    private void OnDisable()
    {
        UIMenu.OnMenu -= OnMenu;
        UIMenu.OnExit += OnExit;
        FinishMainBehaviour.OnFinish -= OnFinish;
        PlayerMainBehaviour.OnDeath -= OnDeathPlayer;
        PlayerNavMeshBehaviour.OnFinish -= OnFinish;
        PlayerMovementBehaviour.OnFinish -= OnFinish;
        UILoadiongScreen.OnLoadingScreen -= OnLoadingScreen;
    }

    private void OnMenu(bool isMenu)
    {
        if (isMenu)
        {
            Time.timeScale = 0;
            SetState(GameStateType.pause);
        }
        else
        {
            Time.timeScale = 1;
            SetState(GameStateType.resume);
        }
    }
    private void OnExit()
    {
        SetState(GameStateType.quit);
    }

    private void OnFinish()
    {
        SetState(GameStateType.finish);
    }

    private void OnDeathPlayer()
    {
        SetState(GameStateType.restart);
    }
    private void OnRestartLevel()
    {
        SetState(GameStateType.resume);
    }

    private void OnLoadingScreen(bool isShow)
    {
        if (!isShow)
            SetState(GameStateType.loadingNewLevel);
    }

    private void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
