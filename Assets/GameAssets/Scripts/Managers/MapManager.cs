using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MapManager : MonoBehaviour, IManager
{
    [SerializeField] private Transform cellParent;
    [SerializeField] private Transform generatingMapParent;
    [SerializeField] private Vector2 amount;
    [SerializeField] private float spacing;

    private static List<CellMainBehaviour> cells = new List<CellMainBehaviour>();

    #region Coordinates
    public static string GetName(Vector2 point)
    {
        foreach (var item in cells)
        {
            if (item.coordinates == point)
            {
                return item.gameObject.name;
            }
        }
        return "";
    }
    public static Vector3 GetPosition(Vector2 point)
    {
        foreach (var item in cells)
        {
            if (item.coordinates == point)
            {
                return new Vector3(item.transform.position.x, 0, item.transform.position.z);
            }
        }
        return Vector3.zero;
    }
    public static Vector2 GetCoordinates(Vector3 point)
    {
        foreach (var item in cells)
        {
            if (item.transform.position == new Vector3(point.x, item.transform.position.y, point.z))
            {
                return new Vector2(item.coordinates.x, item.coordinates.y);
            }
        }
        return Vector2.zero;
    }
    #endregion


    public void Initialize()
    {
        InitializeCell();
    }

    private void InitializeCell()
    {
        float nX = 0;
        float nY = 0;
        int horizontal = 0;
        int vertical = 0;

        var poolManager = GameManager.Instance.GetManager<PoolManager>();
        for (int y = 0; y < amount.y; y++)
        {
            vertical++;
            for (int x = 0; x < amount.x; x++)
            {
                horizontal++;
                var cell = poolManager.GetObject<CellMainBehaviour>().GetComponent<CellMainBehaviour>();
                cell.transform.position = new Vector3(cellParent.position.x - nX, 0, cellParent.position.z - nY);
                cell.transform.SetParent(cellParent);
                cell.SetCoordinates(new Vector2(vertical, horizontal));

                cells.Add(cell);
                nX += spacing;

                var directionWalls = new List<Vector3>();

                if (horizontal == amount.x)
                {
                    directionWalls.Add(Vector3.right);
                }
                if (vertical == 1)
                {
                    directionWalls.Add(Vector3.back);
                }
                if (vertical == amount.y)
                {
                    directionWalls.Add(Vector3.forward);
                }
                if (horizontal == 1)
                {
                    directionWalls.Add(Vector3.left);
                }

                cell.ShowWall(directionWalls);
            }
            horizontal = 0;
            nX = 0;
            nY += spacing;
        }
    }

    #region GenerationMap
    public void GenerationMap(DifficultyLevelType levelType)
    {
        SpawnObstacles(levelType);
        SpawFinish();
        SpawnPlayer();
    }
    public void SpawnPlayer()
    {
        var player = GameManager.Instance.GetManager<PoolManager>().GetObject<PlayerMainBehaviour>().GetComponent<PlayerMainBehaviour>();
        player.gameObject.SetActive(false);
        player.transform.position = (Vector3.up / 6) + cells[0].transform.position;
        player.SetTargetPosition(new Vector3(cells[cells.Count - 1].transform.position.x, player.transform.position.y, cells[cells.Count - 1].transform.position.z));
        StartCoroutine(SpawnPlayerCoroutine(player.gameObject));
        
    }
    private IEnumerator SpawnPlayerCoroutine(GameObject player)
    {
        yield return null;
        player.SetActive(true);
    }

    private void SpawnObstacles(DifficultyLevelType levelType)
    {
        var obstaclesTypes = (ObstaclesType[])Enum.GetValues(typeof(ObstaclesType));
        var random = new System.Random();
        var angles = new Quaternion[4]
        {
            Quaternion.Euler(0, 0, 0),
            Quaternion.Euler(0, 90, 0),
            Quaternion.Euler(0, 180, 0),
            Quaternion.Euler(0, 270, 0),
        };

        var currentObstaclesType = ObstaclesType.nothing;
        var poolManager = GameManager.Instance.GetManager<PoolManager>();

        ObstaclesMainBehaviour obstaclesMainBehaviour = null;

        int intervalTrap = 1 / (int)levelType;
        int countTrap = 0;

        for (int i = 0; i < cells.Count; i++)
        {
            currentObstaclesType = obstaclesTypes[random.Next(obstaclesTypes.Length)];

            if (countTrap == intervalTrap)
            {
                countTrap = 0;
            }
            else
            {
                currentObstaclesType = GetObstaclesType(levelType, currentObstaclesType);
                countTrap++;
            }

            if (currentObstaclesType == ObstaclesType.nothing)
                currentObstaclesType = ObstaclesType.road;

            if (i == cells.Count - 1 || cells[i].coordinates == new Vector2(1, 1) || cells[i].coordinates == new Vector2(2, 1) || cells[i].coordinates == new Vector2(amount.x - 1, amount.y) || cells[i].coordinates == new Vector2(amount.x, amount.y) || cells[i].coordinates == new Vector2(amount.x, amount.y - 1))
            {
                currentObstaclesType = ObstaclesType.road;
            }

            obstaclesMainBehaviour = poolManager.GetObstaces(currentObstaclesType).GetComponent<ObstaclesMainBehaviour>();

            obstaclesMainBehaviour.transform.position = cells[i].transform.position;
            obstaclesMainBehaviour.transform.rotation = angles[UnityEngine.Random.Range(0, angles.Length)];
            obstaclesMainBehaviour.transform.SetParent(generatingMapParent);

            cells[i].SetObstacles(obstaclesMainBehaviour);
        }
    }
    private ObstaclesType GetObstaclesType(DifficultyLevelType levelType, ObstaclesType currentObstaclesType)
    {
        switch (levelType)
        {
            case DifficultyLevelType.nothing:
                break;
            case DifficultyLevelType.easy:

                switch (currentObstaclesType)
                {
                    case ObstaclesType.nothing:
                        currentObstaclesType = ObstaclesType.road;
                        break;
                    case ObstaclesType.road:
                        break;
                    case ObstaclesType.roadWall:
                        break;
                    case ObstaclesType.fireRoad:
                        currentObstaclesType = ObstaclesType.roadWall;
                        break;
                    default:
                        break;
                }

                break;
            case DifficultyLevelType.medium:
                break;
            case DifficultyLevelType.hard:
                break;
            default:
                break;
        }

        return currentObstaclesType;
    }

    private void SpawFinish()
    {
        var finish = GameManager.Instance.GetManager<PoolManager>().GetObject<FinishMainBehaviour>().GetComponent<FinishMainBehaviour>();
        finish.transform.position = (Vector3.up / 6) + cells[cells.Count - 1].transform.position;
        finish.SetCoordinates(cells[cells.Count - 1].coordinates);
    }
    #endregion

    private void OnDestroy()
    {
        cells.Clear();
    }
}
