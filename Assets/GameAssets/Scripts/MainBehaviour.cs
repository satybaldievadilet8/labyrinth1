using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public abstract class MainBehaviour : MonoBehaviour
{
    [SerializeField] protected List<GameObject> componentInGameObjects;

    protected List<IComponent> components = new List<IComponent>();


    protected virtual void Awake()
    {
        Initailize();
    }
    private void Initailize()
    {
        foreach (var item in componentInGameObjects)
        {
            var components = item.GetComponents<IComponent>();
            foreach (var component in components)
            {
                var main = component as MainBehaviour;
                if (component != null && main == null) 
                    this.components.Add(component);
            }
        }
    }

    protected virtual void OnEnable()
    {
        //ActivateComponent();
    }
    protected virtual void OnDisable()
    {
        //DeactiveComponent();
    }

    private void ActivateComponent()
    {
        foreach (var item in componentInGameObjects)
        {
            IComponent[] components = item.GetComponents<IComponent>();
            foreach (var component in components)
            {
                var mono = component as MonoBehaviour;
                if (!mono) continue;
                mono.enabled = true;
            }
        }
    }
    private void DeactiveComponent()
    {
        foreach (var item in componentInGameObjects)
        {
            IComponent[] components = item.GetComponents<IComponent>();
            foreach (var component in components)
            {
                var mono = component as MonoBehaviour;
                if (!mono) continue;
                mono.enabled = false;
            }
        }
    }


    public T GetComponentInGameObject<T>()
    {
        foreach (var item in componentInGameObjects)
        {
            if (item.GetComponent<T>() != null)
                return item.GetComponent<T>();
        }
        return default;
    }
}
